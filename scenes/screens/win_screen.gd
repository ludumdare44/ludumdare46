extends Node

export (PackedScene) var scene_to_load


func _process(delta):
	if Input.is_action_just_pressed('action'):
		_on_Button_pressed()

func _on_Button_pressed():
	AudioManager.play_sound_effect('start_game')
	$fader.current_animation = 'fade'

func _fade_finished():
	get_tree().change_scene_to(scene_to_load)
