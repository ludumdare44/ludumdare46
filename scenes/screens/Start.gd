extends Node

export(PackedScene) var scene_to_load

onready var fader = $fader

func _ready():
	Pathfinding.set_tree(get_tree())
	Pathfinding.init()

func _process(delta):
	if Input.is_action_just_pressed("action"):
		_on_Button_pressed()

func _on_Button_pressed():
	print("-----------Start------------")
	AudioManager.play_sound_effect("start_game")
	fader.current_animation = "fade"

# note this gets called at the end of the fade animation
func _fade_finished():
	get_tree().change_scene_to(scene_to_load)
