extends Node

const GRID_SIZE = 16
var _grid_size = Vector2(0, 0)

# Set by the init method, since this singleton's _ready method is called
# before that of the main scene
var _tree = null
var _viewport = null
var _init_complete = false

var _disconnected = PoolIntArray()
var _astar = AStar.new()

func point2index(point: Vector2) -> int:
	return point.y * _grid_size.x + point.x

func index2point(idx: int) -> Vector2:
	return Vector2(idx % int(_grid_size.x), int(idx / int(_grid_size.x)))

func _ready():
	pass

func set_tree(tree):
	_tree = tree

# Called by the main scene's script, since this singleton's _ready method is
# called before that of the main scene.
func init():
	if !_init_complete:
		print("init()")
		_viewport = _tree.get_root()
		#_saved_vertices = $floor.get_vertices()
		_grid_size.x = ceil(_viewport.size.x / GRID_SIZE)
		_grid_size.y = ceil(_viewport.size.y / GRID_SIZE)
		# Set all cells to unoccupied
		for i in range(0, _grid_size.x * _grid_size.y):
			var point = index2point(i)
			_astar.add_point(i, Vector3(point.x, point.y, 0))
			if point.x > 0:
				_astar.connect_points(i, point2index(Vector2(point.x - 1, point.y)))
			if point.y > 0:
				_astar.connect_points(i, point2index(Vector2(point.x, point.y - 1)))
		#for i in range(0, _grid_size.x * _grid_size.y):
		#	var point = index2point(i)
		#	_connect_neighbors(point)
		_init_complete = true

func _get_neighbors(point: Vector2) -> PoolVector2Array:
	var neighbors = PoolVector2Array()
	if point.x > 0:
		neighbors.append(Vector2(point.x - 1, point.y))
	if point.y > 0:
		neighbors.append(Vector2(point.x, point.y - 1))
	if point.x < (_viewport.size.x / GRID_SIZE):
		neighbors.append(Vector2(point.x + 1, point.y))
	if point.y < (_viewport.size.y / GRID_SIZE):
		neighbors.append(Vector2(point.x, point.y + 1))
	return neighbors

func _connect_neighbors(point: Vector2):
	var idx = point2index(point)
	for neighbor in _get_neighbors(point):
		var neighbor_idx = point2index(neighbor)
		if not _astar.are_points_connected(idx, neighbor_idx):
			_astar.connect_points(idx, neighbor_idx)

func _disconnect_neighbors(point: Vector2):
	var idx = point2index(point)
	for neighbor in _get_neighbors(point):
		var neighbor_idx = point2index(neighbor)
		if _astar.are_points_connected(idx, neighbor_idx):
			_astar.disconnect_points(idx, neighbor_idx)
			#print("Disconnect ", point, " from ", neighbor)

# convert viewport space -> grid space
func world2map(point: Vector2) -> Vector2:
	return Vector2(int(point.x / GRID_SIZE), int(point.y / GRID_SIZE))

func map2world(point: Vector2) -> Vector2:
	return GRID_SIZE * point

func _intersect(obj: Object = null):
	# clear previous disconnections
	for idx in _disconnected:
		_connect_neighbors(index2point(idx))
	_disconnected.resize(0)

	var dancers = _tree.get_nodes_in_group('dancers')
	var collide_shapes = []
	var collide_transforms = []
	for dancer in dancers:
		if dancer != obj:
			collide_shapes.append(dancer.find_node('collision_shape').shape)
			collide_transforms.append(dancer.transform)

	var obstacles = _tree.get_nodes_in_group('obstacles')
	for obstacle in obstacles:
		collide_shapes.append(obstacle.find_node('collision_shape').shape)
		collide_transforms.append(obstacle.transform)

	#var floor_node = _tree.get_root().find_node('floor', true, false)
	#var floor_shape = floor_node.find_node('shape')

	var shape = RectangleShape2D.new()
	shape.set_extents(Vector2(GRID_SIZE, GRID_SIZE))
	for i in _grid_size.x * _grid_size.y:
		var point = index2point(i)
		var transform = Transform2D(
				0.0, Vector2(GRID_SIZE * point.x, GRID_SIZE * point.y))
		#if not shape.collide(transform, floor_shape, floor_node.transform):
		#	_disconnect_neighbors(index2point(i))
		#	_disconnected.append(i)
		# Check if the cell collides with any of the dancers
		for j in range(collide_shapes.size()):
			if shape.collide(transform, collide_shapes[j], collide_transforms[j]):
				_disconnect_neighbors(index2point(i))
				_disconnected.append(i)

func find_path(from: Vector2, to: Vector2, obj: Object = null):
	if not _init_complete:
		return

	# Assume the incoming vectors are relative to the viewport
	var scaled_from = world2map(from)
	var scaled_to = world2map(to)
	#print("from: ", from, " / ", scaled_from, " / ", point2index(scaled_from), " to ", to, " / ", scaled_to, " / ", point2index(scaled_to))
	_intersect(obj)
	var path_3d = _astar.get_point_path(point2index(scaled_from), point2index(scaled_to))
	var path = PoolVector2Array()
	path.resize(path_3d.size())
	for i in range(path_3d.size()):
		path[i] = map2world(Vector2(path_3d[i].x, path_3d[i].y))
	print("path: ", path)
	return path
