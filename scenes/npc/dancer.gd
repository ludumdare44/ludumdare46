extends KinematicBody2D


# When in the DANCING or IDLE state, this is the probability of the Dancer
# moving each tick.
const MOVE_PROBABILITY = 0.2

const MAX_VELOCITY = 300
const ACCELERATION = 800
const FRICTION = 200

# Number in [0, 1). When each Dancer is spawned, a random number will be chosen
# from a uniform distrubtion with the below parameters. That number will be
# multiplied by the distance from the door to the bottom right of the
# screen--defined as the target distance. The Dancer will leave the "entering"
# state when the target distance is reached.
const MIN_SPAWN_DISTANCE = 0.3
const MAX_SPAWN_DISTANCE = 0.8

# In seconds, this is how long the Dancer will wait after arriving at a
# destination to pick a new destination to navigate to
const PATH_RATE = 40

# Try this many times to choose a new path (pick a point; if no path to that
# point exists, pick another point this many times).
const PATH_TRIES = 3

# Enumerations
enum State {
		DANCING,
		IDLE,
		MAX,
		}

enum Direction {
		UP,
		RIGHT,
		DOWN,
		LEFT,
		}

# This represents types of happiness metrics
enum SparkJoy {
		DRINK,
		DANCE,
		FOOD,
		MAX,
		}

enum HappinessState {
		HAPPIEST,
		HAPPY,
		NEUTRAL,
		UNHAPPY,
		UNHAPPIEST,
		}

# Dancer attributes
var velocity = Vector2.ZERO
var _direction = Direction.RIGHT
var happiness_tick_rate = 3
var drunk_tick_rate = 3
var turn_tick_rate = 1
var _happiness_attrs = {
		SparkJoy.DRINK: 100,
		SparkJoy.DANCE: 100,
		SparkJoy.FOOD: 100,
		}
# Various counters
# Drunkenness from 1 to 10, float
var _drunkness: float = 0

func _is_drunk():
	return _drunkness > 10

# How much does each timer tick deduct from each type of happiness?
const _happiness_rate = {
		SparkJoy.DRINK: 5,
		SparkJoy.DANCE: 5,
		SparkJoy.FOOD: 5,
		}

# How much does drunkenness (float, on a scale from 1 to 10) decrease each
# tick?
const _drunk_rate: float = 0.5

const _animation = {
		State.IDLE: 'idle_animation',
		State.DANCING: 'dancing_animation',
		}

const _directional_animation = [
		{
			Direction.UP: 'Dancer_1_n',
			Direction.RIGHT: 'Dancer_1_e',
			Direction.DOWN: 'Dancer_1_s',
			Direction.LEFT: 'Dancer_1_w',
		},
		{
			Direction.UP: 'Dancer_2_n',
			Direction.RIGHT: 'Dancer_2_e',
			Direction.DOWN: 'Dancer_2_s',
			Direction.LEFT: 'Dancer_2_w',
		},
	]

# This determines what dancer image we use:
# - 0: Dancer_1
# - 1: Dancer_2
var _animation_idx = -1

var _rng = RandomNumberGenerator.new()
var _state = State.DANCING
var _target_distance = null
var _happiness_timer = null
var _drunk_timer = null
var _turn_timer = null

var _path = null
var _path_timeout = 0

onready var happiness_sprite = $happinessSprite

# When not equal, the entity's happiness hasn't been presented/flashed to the
# user, so it should be flashed at the next opportunity.
var _happiness_state = HappinessState.HAPPY
var _last_happiness_state = HappinessState.HAPPY
var _happiness_shown = false

# Called when the node enters the scene tree for the first time.
func _ready():
	_rng.randomize()

	add_to_group('dancers')

	# Pick which animation to use
	var num_animations = _directional_animation.size()
	_animation_idx = _rng.randi_range(0, num_animations - 1)
	for i in range(State.MAX):
		var animations = find_node(_animation[i], true, false)
		animations.set_animation(
				_directional_animation[_animation_idx][_direction])

	# Figure out how far we're going to travel when we enter the room
	var viewport = get_tree().get_root()
	var room = viewport.find_node('root', true, false)
	var door = room.find_node('door')
	var door_distance = viewport.size.distance_to(door.position)
	_target_distance = door_distance * _rng.randf_range(
			MIN_SPAWN_DISTANCE, MAX_SPAWN_DISTANCE)

	# Set up the happiness timer and hook it to its callback
	_happiness_timer = Timer.new()
	_happiness_timer.connect('timeout', self, '_happiness_timer_cb')
	_happiness_timer.set_wait_time(happiness_tick_rate)
	_happiness_timer.set_autostart(true)
	add_child(_happiness_timer)

	# Set up the drunkenness timer and hook it to its callback
	_drunk_timer = Timer.new()
	_drunk_timer.connect('timeout', self, '_drunk_timer_cb')
	_drunk_timer.set_wait_time(drunk_tick_rate)
	_drunk_timer.set_autostart(true)
	add_child(_drunk_timer)

func _physics_process(delta):
	_path_timeout -= delta
	if _path_timeout <= 0:
		var viewport = get_tree().get_root()
		var floor_area = viewport.find_node('floor', true, false).find_node('area')
		var floor_extents = floor_area.shape.extents
		for i in range(PATH_TRIES):
			var dest_point = floor_area.position + Vector2(
					_rng.randf_range(-(floor_extents.x / 2), floor_extents.x / 2),
					_rng.randf_range(-(floor_extents.y / 2), floor_extents.y / 2))
			_path = Pathfinding.find_path(position, dest_point, self)
			_path_timeout = PATH_RATE
			if _path != null and _path.size() > 0:
				break
	if _path == null:
		velocity = move_and_slide(velocity.move_toward(
			Vector2.ZERO, FRICTION * delta))
	elif _path.size() == 0:
		_path = null
		_path_timeout = PATH_RATE
	else:
		var point = _path[0]
		var delta_pos = point - position
		if delta_pos.length() < 16:
			_path.remove(0)
		velocity = move_and_slide(velocity.move_toward(
			delta_pos,
			ACCELERATION * delta))
		_update_direction()

func _update_direction():
	var angle = rad2deg(velocity.angle())
	#print(angle)
	var new_direction = _direction
	if angle < -90:
		_direction = Direction.LEFT
	elif angle < 0:
		_direction = Direction.UP
	elif angle < 90:
		_direction = Direction.RIGHT
	else:
		_direction = Direction.DOWN
	var animations = find_node(_animation[_state], true, false)
	animations.set_animation(
			_directional_animation[_animation_idx][_direction])

func _process(delta):
	transition_state()
	if DancerState.status_shown:
		happiness_sprite.set_visible(true)
	else:
		happiness_sprite.set_visible(false)

func transition_state():
	if _last_happiness_state == HappinessState.NEUTRAL \
			and _happiness_state == HappinessState.UNHAPPY:
		find_node(_animation[State.DANCING]).set_visible(false)
		find_node(_animation[State.IDLE]).set_visible(true)
		return State.IDLE
	elif _last_happiness_state == HappinessState.NEUTRAL \
			and _happiness_state  == HappinessState.HAPPY:
		find_node(_animation[State.IDLE]).set_visible(false)
		find_node(_animation[State.DANCING]).set_visible(true)
		return State.DANCING
	else:
		return _state

func choose_direction():
	match _rng.randi_range(0, 3):
		0:
			return Direction.UP  # up
		1:
			return Direction.RIGHT  # right
		2:
			return Direction.DOWN  # down
		3:
			return Direction.LEFT  # left

func choose_direction_right_bias():
	var rand = _rng.randf()
	if rand < 0.01:
		return Direction.LEFT  # left
	elif rand < 0.31:
		return Direction.UP  # up
	elif rand < 0.61:
		return Direction.DOWN  # down
	else:
		return Direction.RIGHT  # right

func direction_to_vector(direction):
	match direction:
		Direction.UP:
			return Vector2(0, -1)  # up
		Direction.RIGHT:
			return Vector2(1, 0)  # right
		Direction.DOWN:
			return Vector2(0, 1)  # down
		Direction.LEFT:
			return Vector2(-1, 0)  # left

func summarize_happiness():
	if _is_drunk():
		print("Drunk and unhappy as can be! Get them water!")
		return HappinessState.UNHAPPIEST
	return _happiness_attrs[SparkJoy.DRINK]

func _happiness_timer_cb():
	for key in _happiness_attrs:
		_happiness_attrs[key] = max(
				0, _happiness_attrs[key] - _happiness_rate[key])
	_last_happiness_state = _happiness_state
	var summary = summarize_happiness()
	if summary < 20:
		_happiness_state = HappinessState.UNHAPPIEST
	elif summary < 40:
		_happiness_state = HappinessState.UNHAPPY
	elif summary < 60:
		_happiness_state = HappinessState.NEUTRAL
	elif summary < 80:
		_happiness_state = HappinessState.HAPPY
	else:
		_happiness_state = HappinessState.HAPPIEST

	if _happiness_state != _last_happiness_state:
		happiness_sprite.frame = _happiness_state
		happiness_sprite.set_visible(true)
	elif not DancerState.status_shown:
		happiness_sprite.set_visible(false)

func _drunk_timer_cb():
	_drunkness = max(0, _drunkness - _drunk_rate)

func _accept_item(item):
	AudioManager.play_sound_effect("give_drink")
	print("Item given to dancer, drunkness was ", _drunkness)
	match item:
		Inventory.ItemType.BEER:
			_drunkness += 1
			_happiness_attrs[SparkJoy.DRINK] += 10
		Inventory.ItemType.SHOT:
			_drunkness += 2
			_happiness_attrs[SparkJoy.DRINK] += 10
		Inventory.ItemType.WATER:
			StatusText.sayWater()
			_drunkness -= 2
			# resets to neutral no matter what; will remain sick until
			# _drunkness < 10
			_happiness_attrs[SparkJoy.DRINK] = 60

	if item == Inventory.ItemType.BEER or item == Inventory.ItemType.SHOT:
		if _drunkness < 7:
			StatusText.sayThanks()
		else:
			StatusText.sayTooMuch()

	print("Drunkness is now ", _drunkness)
	_happiness_timer_cb()
	


func _on_hurtbox_area_entered(area):
	if Inventory.takeActiveItem():
		_accept_item(Inventory.getActiveItem())

