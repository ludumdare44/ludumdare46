extends CanvasItem

onready var animation = $AnimationPlayer

func _ready():
	animation.current_animation = "change"
	visible = true
