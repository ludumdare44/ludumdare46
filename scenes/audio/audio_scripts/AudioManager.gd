extends Node

func play_song(song):
	get_node("/root/AudioManager/music_title").stop()
	get_node("/root/AudioManager/music_bgm").stop()
	get_node("/root/AudioManager/music_gameover").stop()

	var node_to_play = get_node("/root/AudioManager/"+song)
	node_to_play.play()

func play_sound_effect(sound_name):
	var node_to_play = get_node("/root/AudioManager/SFX/"+sound_name)
	node_to_play.play()
