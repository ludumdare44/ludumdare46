extends Node


onready var _happy_icon = $HappyBars/Bar1/Count/Background/Title
onready var _text = $HappyBars/Bar1/NinePatchRect/StausText


func _ready():
	StatusText.connect("StatusTextChange", self, "_text_change")


func _text_change():
	_happy_icon.set_frame(StatusText.getActiveStatus())
	_text.text = StatusText.getActiveText()
	print("Text changed!")
