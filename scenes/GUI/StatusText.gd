extends Node

#signal people can subscribe to for changes
signal StatusTextChange

enum STATUS{
	HAPPY = 1,
	COOL = 4,
	TOO_MUCH = 5,
	SLIPPED = 3,
}

var _ActiveText = ""
var _ActiveStatus = STATUS.HAPPY
var _lastThanks = 0
var _lastDrunk = 0
var _lastWater = 0

func _ready():
	randomize()

# sayings
const _ThanksStrings = [
	"Thanks!",
	"Groovy",
	"Thanks brocacho",
	"Far out!",
	"I can dig it",
	"Right on",
	"Bomb, dude",
	"Bitch'n",
	"Cool",
	"Rad",
	"Radical",
	"Signed, sealed, delivered",
]

const _TooDrunkStrings = [
	"Oh god...",
	"This is too much",
	"*silence*",
	"Looookin' goooood!!!",
	"*stares into space*",
	"Meeeeoooooooowwww!",
	"...burpp",
	"I'm spinning out!",
	"Slammmed drunk",
	"Check you later",
]

const _WaterStrings = [
	"Whew",
	"What a square. Thanks, though",
	"Smooth operator",
	"Chilling out",
	"Cool",
	"Mellow out",
	"Tight!",
	"Feelin' zen",
]


# access functions
func getActiveText():
	return _ActiveText

func getActiveStatus():
	return _ActiveStatus

func sayThanks():
	_ActiveStatus = STATUS.HAPPY
	var candidate = 0
	while(candidate == _lastThanks):
		candidate = randi() % _ThanksStrings.size()
	_lastThanks = candidate
	_ActiveText = _ThanksStrings[candidate]
	self.emit_signal("StatusTextChange")

func sayTooMuch():
	_ActiveStatus = STATUS.TOO_MUCH
	var candidate = 0
	while(candidate == _lastDrunk):
		candidate = randi() % _TooDrunkStrings.size()
	_lastDrunk = candidate
	_ActiveText = _TooDrunkStrings[candidate]
	emit_signal("StatusTextChange")

func sayWater():
	_ActiveStatus = STATUS.COOL
	var candidate = 0
	while(candidate == _lastWater):
		candidate = randi() % _WaterStrings.size()
	_lastWater = candidate
	_ActiveText = _WaterStrings[candidate]
	emit_signal("StatusTextChange")

func saySlip():
	_ActiveStatus = STATUS.SLIPPED
	_ActiveText = "wwwWWWwwwooaaAAhhhhhhhh!!!!"
	emit_signal("StatusTextChange")

