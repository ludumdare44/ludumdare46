extends Node

onready var _beer_node = $HBoxContainer/BEER/Background/Number
onready var _shot_node = $HBoxContainer/SHOT/Background/Number
onready var _water_node = $HBoxContainer/WATER/Background/Number
onready var _happy_gauge = $HBoxContainer/HappyBars/Bar1/gauge
onready var _happy_number = $HBoxContainer/HappyBars/Bar1/Count/Background/Number
onready var _happy_icon = $HBoxContainer/HappyBars/Bar1/Count/Background/Title

onready var _beer_bg = $HBoxContainer/BEER/Background
onready var _shot_bg = $HBoxContainer/SHOT/Background
onready var _water_bg = $HBoxContainer/WATER/Background

# Called when the node enters the scene tree for the first time.
func _ready():
	pass

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if _beer_node:
		_beer_node.set_text(str(Inventory.numItem(Inventory.ItemType.BEER)))

	if _shot_node:
		_shot_node.set_text(str(Inventory.numItem(Inventory.ItemType.SHOT)))

	if _water_node:
		_water_node.set_text(str(Inventory.numItem(Inventory.ItemType.WATER)))

	match Inventory.getActiveItem():
		Inventory.ItemType.BEER:
			_beer_bg.show()
			_shot_bg.hide()
			_water_bg.hide()
		Inventory.ItemType.SHOT:
			_beer_bg.hide()
			_shot_bg.show()
			_water_bg.hide()
		Inventory.ItemType.WATER:
			_beer_bg.hide()
			_shot_bg.hide()
			_water_bg.show()
	if _water_node:
		_water_node.set_text(str(Inventory.numItem(Inventory.ItemType.WATER)))

	var summary = 100.0
	var dancers = get_tree().get_nodes_in_group('dancers')
	for dancer in dancers:
		var dancer_summary = dancer.summarize_happiness()
		if dancer_summary < summary:
			summary = dancer_summary
			if summary == 0:
				get_tree().change_scene('res://scenes/screens/lose_screen.tscn')

	_happy_gauge.set_value(summary)
	_happy_number.set_text(str(int(summary / 10)))
	_happy_icon.set_frame(int((100 - summary) / 20))
