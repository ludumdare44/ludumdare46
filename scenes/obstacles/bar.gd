extends Node



# use action to get the active inventory item
func _on_hurtbox_area_entered(area):
	AudioManager.play_sound_effect("pour_drink")
	
	Inventory.addItem(Inventory.ItemType.BEER, 1)
	print("got a beer")
	print("beers = " + str(Inventory.numItem(Inventory.ItemType.BEER)))
