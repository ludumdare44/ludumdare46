extends Node

export (Inventory.ItemType) var drinkType

# use action to get the active inventory item
func _on_hurtbox_area_entered(area):
	AudioManager.play_sound_effect("pour_drink")
	Inventory.setActiveItem(drinkType)
	match Inventory.addItem(drinkType, 1):
		Inventory.ItemType.BEER:
			print("got a beer")
			print("beers = " + str(Inventory.numItem(Inventory.ItemType.BEER)))
		Inventory.ItemType.SHOT:
			print("got a shot")
			print("shots = " + str(Inventory.numItem(Inventory.ItemType.SHOT)))
		Inventory.ItemType.WATER:
			print("got a water")
			print("water = " + str(Inventory.numItem(Inventory.ItemType.WATER)))
