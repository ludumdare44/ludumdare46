extends KinematicBody2D

#DEFINES
const ACCELERATION = 1000
const MAX_SPEED = 500
const NORMAL_FRICTION = 2000
const SLIPPERY_FRICTION = 100
const NORMAL_TRAIL = 200
const CRAZY_TRAIL = 1000

const RING_COLOR_NORMAL = Color("34dec3")
const RING_COLOR_SLIP = Color.brown

var friction = NORMAL_FRICTION
var velocity = Vector2.ZERO

enum {
	MOVE,
	ACTION,
	SLIP
}
var state = MOVE

onready var animationPlayer = $AnimationPlayer
onready var animationTree = $AnimationTree
onready var animationState = animationTree.get("parameters/playback")

onready var trail = $trail
onready var ring = $ring

onready var slipTimer = $player_effects/slipTimer

# Called when the node enters the scene tree for the first time.
func _ready():
	animationTree.active = true
	trail.initial_velocity = NORMAL_TRAIL
	ring.modulate = RING_COLOR_NORMAL

# physics callback
func _physics_process(delta):
	match state:
		MOVE:
			move_state(delta)
		ACTION:
			action_state(delta)
		SLIP:
			move_state(delta)


# handle all movement
func move_state(delta):

	#this is in here to prevent what I think was a race condition
	if Input.is_action_just_pressed("action"):
		state = ACTION

	var input_vector = Vector2.ZERO
	if state != SLIP:
		input_vector.x = Input.get_action_strength("ui_right") - Input.get_action_strength("ui_left")
		input_vector.y = Input.get_action_strength("ui_down") - Input.get_action_strength("ui_up")
		input_vector = input_vector.normalized()

	if input_vector != Vector2.ZERO:
		animationTree.set("parameters/Idle/blend_position", input_vector)
		animationTree.set("parameters/Walk/blend_position", input_vector)
		animationTree.set("parameters/Action/blend_position", input_vector)
		animationState.travel("Walk")
		
		trail.direction = input_vector * -1
		
		velocity = velocity.move_toward(input_vector * MAX_SPEED, ACCELERATION * delta)
	else:
		animationState.travel("Idle")
		
		trail.direction = input_vector
		velocity = velocity.move_toward(Vector2.ZERO, friction * delta)

	trail.emitting = velocity != Vector2.ZERO
	velocity = move_and_slide(velocity)

# handle actions
func action_state(delta):
	$Position2D/hitbox/CollisionShape2D2.disabled = false
	velocity = Vector2.ZERO
	animationState.travel("Action")
	trail.emitting = false
	
	
func action_finished(): 
	$Position2D/hitbox/CollisionShape2D2.disabled = true
	state = MOVE


# handlers for the slippry cup
func _on_player_effects_area_shape_entered(area_id, area, area_shape, self_shape):
	friction = SLIPPERY_FRICTION
	slipTimer.start()
	trail.initial_velocity = CRAZY_TRAIL
	ring.modulate = RING_COLOR_SLIP
	StatusText.saySlip()
	AudioManager.play_sound_effect("slip")
	state = SLIP
	print("Slipped!")

func _on_slipTimer_timeout():
	friction = NORMAL_FRICTION
	trail.initial_velocity = NORMAL_TRAIL
	ring.modulate = RING_COLOR_NORMAL
	state = MOVE
	print("recovered")

