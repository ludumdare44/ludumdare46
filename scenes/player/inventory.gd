extends Node

enum ItemType {
		BEER,
		SHOT,
		WATER,
		NUMTYPES
	}

var _MaxAmount = {
	ItemType.BEER: 10,
	ItemType.SHOT: 20,
	ItemType.WATER: 10
}

var _CurrentAmount = {
	ItemType.BEER: 0,
	ItemType.SHOT: 0,
	ItemType.WATER: 0
}

var _ActiveItem = ItemType.BEER

func setActiveItem(type):
	_ActiveItem = type

func getActiveItem():
	return _ActiveItem

# For adding to the inventory
func addItem(itemType, num):
	if _CurrentAmount[itemType] != _MaxAmount[itemType]:
		_CurrentAmount[itemType] = min(_MaxAmount[itemType], _CurrentAmount[itemType] + num)

func addActiveItem(num):
	addItem(_ActiveItem, num)
	return _ActiveItem   # so the caller knows which type of item they added

func takeActiveItem():
	if _CurrentAmount[_ActiveItem] > 0:
		_CurrentAmount[_ActiveItem] -= 1
		return true
	return false

func numItem(item):
	return _CurrentAmount[item]


	

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if Input.is_action_just_released("active_item_right"):
		AudioManager.play_sound_effect("select_active_drink")
		var next_state = getActiveItem() + 1
		if next_state == ItemType.NUMTYPES:
			next_state = ItemType.BEER
		setActiveItem(next_state)
	elif Input.is_action_just_released("active_item_left"):
		AudioManager.play_sound_effect("select_active_drink")
		var next_state = getActiveItem() - 1
		if next_state < 0:
			next_state = ItemType.NUMTYPES-1
		setActiveItem(next_state)
