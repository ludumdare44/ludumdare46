extends Node2D


# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
	Pathfinding.set_tree(get_tree())
	Pathfinding.init()


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass


func _on_Button_pressed(): #change this to on_game_over
	get_tree().change_scene('res://scenes/screens/title_screen.tscn')
	pass # Replace with function body.
