extends Node2D

onready var popup = $popup
onready var hideButton = $popup/Hide

var showing = false

func _ready():
	popup.visible = false
	
func _process(delta):
	if Input.is_action_just_pressed("action") and showing:
		_on_Hide_pressed()

func _on_Show_pressed():
	popup.visible = true
	hideButton.disabled = false
	showing = true

func _on_Hide_pressed():
	popup.visible = false
	hideButton.disabled = true
	showing = false
