extends RichTextLabel

var s = 0
var m = 0
var h = 10

func _get_str():
	var m_str = str(m)
	if m_str.length() == 1:
		m_str = '0' + m_str
	return str(h) + ':' + m_str

func _process(delta):
	if s > 3 :
		h +=1
		s = 0

	m = s*15
	set_text(_get_str())

	if h >= 16:
		get_tree().change_scene('res://scenes/screens/win_screen.tscn')


func _on_ms_timeout():
	s += 1
