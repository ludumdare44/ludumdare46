extends Area2D

# Probability of spawning a new dancer each second
export (float) var spawn_probability = 0.05

# Max number of dancers that will be spawned
export (int) var max_dancers = 5;

# Rate at which happiness metrics are deducted, in seconds
export (int) var happiness_tick_rate = 5

var _rng = RandomNumberGenerator.new()
var _dancer_scene = load('res://scenes/npc/dancer.tscn')
var spawned = 0


# Called when the node enters the scene tree for the first time.
func _ready():
	_rng.randomize()

func spawn_dancers():
	if spawned < max_dancers:
		var rand = _rng.randf()
		if rand < spawn_probability:
			var dancer = _dancer_scene.instance()
			get_parent().add_child(dancer)
			dancer.position = position
			dancer.happiness_tick_rate = happiness_tick_rate
			spawned += 1
